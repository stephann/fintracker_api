require 'rails_helper'

RSpec.describe Record, type: :model do
  describe 'Columns' do
    it { is_expected.to have_db_column(:amount_cents).of_type(:integer) }
    it { is_expected.to have_db_column(:amount_currency).of_type(:string) }
    it { is_expected.to monetize(:amount) }
    it { is_expected.to have_db_column(:occurrence_date).of_type(:datetime) }
    it { is_expected.to have_db_column(:description).of_type(:string) }
    it { is_expected.to have_db_column(:group_cd).of_type(:integer) }
  end

  describe 'Relations' do
    it { is_expected.to belong_to(:account) }
    it { is_expected.to have_many(:record_items).dependent(:destroy) }
    it { is_expected.to have_many(:tags).through(:record_items) }
    it { is_expected.to accept_nested_attributes_for(:record_items).allow_destroy(true) }
  end

  describe 'Enum' do
    it 'has group enum' do
      expect(Record.groups.hash).to eq('expense' => -1, 'income' => 1)
    end
  end

  describe 'Callbacks' do
    it { is_expected.to callback(:assign_calculated_amount_to_amount).before(:validation) }
    it { is_expected.to callback(:update_account_balance).after(:save).if(:amount_cents_changed?) }
    it { is_expected.to callback(:update_account_balance).after(:destroy) }
  end

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:occurrence_date) }
    it { is_expected.to validate_presence_of(:group) }
    it { is_expected.to validate_presence_of(:record_items) }
    it do
      allow(subject).to receive(:assign_calculated_amount_to_amount).and_return(true)
      is_expected.to validate_numericality_of(:amount).is_greater_than(0)
    end
  end

  describe 'Class methods' do
    describe '.total_amount' do
      let!(:record_1) { FactoryBot.create(:record_with_items) }
      let!(:record_2) { FactoryBot.create(:record_with_items) }

      it 'returns a sum of all records amount' do
        expect(Record.total_amount).to eq(record_1.amount + record_2.amount)
      end
    end
  end

  describe 'Instance methods' do
    describe '#assign_calculated_amount_to_amount' do
      let(:record) { FactoryBot.create(:record_with_items) }
      let(:random_amount) { Monetize.parse(Faker::Number.decimal(2)) }

      it 'assigns calculdate amount to amount' do
        old_amount = record.amount
        allow(record).to receive(:calculated_amount).and_return(random_amount)
        record.assign_calculated_amount_to_amount
        expect(record.amount).to eq random_amount
        record.reload
        expect(record.amount).to eq old_amount
      end
    end

    describe '#calculated_amount' do
      let(:record) { FactoryBot.build(:record) }
      let(:record_item_1) { FactoryBot.build(:record_item, record: nil) }
      let(:record_item_2) { FactoryBot.build(:record_item, record: nil) }

      it 'returns sum of record items amount' do
        expect(record.calculated_amount).to be_zero
        record.record_items << record_item_1
        expect(record.calculated_amount).to eq record_item_1.amount
        record.record_items << record_item_2
        expect(record.calculated_amount).to eq record_item_1.amount + record_item_2.amount
      end
    end

    describe '#update_account_balance' do
      let(:record) { FactoryBot.build(:record) }

      context 'when account isn`t marked for destruction' do
        before { allow(record.account).to receive(:marked_for_destruction?).and_return(false) }

        it 'updates account balance' do
          expect(record.account).to receive(:update_balance)
          record.update_account_balance
        end
      end

      context 'when account is marked for destruction' do
        before { allow(record.account).to receive(:marked_for_destruction?).and_return(true) }

        it 'does`t update account balance' do
          expect(record.account).to_not receive(:update_balance)
          record.update_account_balance
        end
      end
    end
  end
end
