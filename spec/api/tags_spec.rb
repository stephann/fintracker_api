require 'rails_helper'

RSpec.describe Stefin::V1::Tags do
  let(:invalid_params) { { name: nil, color: nil } }
  let(:valid_params) do
    FactoryBot.attributes_for(:tag).slice(:name, :color)
  end

  context 'GET /v1/tags' do
    let!(:tag) { FactoryBot.create(:tag) }

    it 'returns tags list' do
      get '/v1/tags'
      expect(response).to have_http_status(:ok)
      expect(response.body).to eq({ tags: [TagEntity.new(tag)] }.to_json)
    end
  end

  context 'GET /v1/tags/:id' do
    let(:tag) { FactoryBot.create(:tag) }

    it 'returns requested tag' do
      get "/v1/tags/#{tag.id}"
      expect(response).to have_http_status(:ok)
      expect(response.body).to eq({ tag: TagEntity.new(tag) }.to_json)
    end
  end

  context 'POST /v1/tags' do
    context 'with valid attributes' do
      it 'creates a new tag' do
        expect do
          post '/v1/tags', params: { tag: valid_params }
        end.to change(Tag, :count).by(1)

        expect(response).to have_http_status(:created)

        expect(response.body).to eq({ tag: TagEntity.new(Tag.last) }.to_json)
      end
    end

    context 'with invalid attributes' do
      it 'doesn`t create a new tag' do
        expect do
          post '/v1/tags', params: { tag: invalid_params }
        end.to_not change(Tag, :count)

        expect(response).to have_http_status(:unprocessable_entity)

        expect(JSON.parse(response.body)['errors']).to_not be_blank
      end
    end
  end

  context 'PUT /v1/tags/:id' do
    let(:tag) { FactoryBot.create(:tag) }

    context 'with valid attributes' do
      it 'updates requested tag' do
        put "/v1/tags/#{tag.id}", params: { tag: valid_params }

        expect(response).to have_http_status(:ok)

        expect(response.body).to eq({ tag: TagEntity.new(tag.reload) }.to_json)

        expect(tag.name).to eq valid_params[:name]
      end
    end

    context 'with invalid attributes' do
      it 'doesn`t update requested tag' do
        put "/v1/tags/#{tag.id}", params: { tag: invalid_params }

        expect(response).to have_http_status(:unprocessable_entity)

        expect(JSON.parse(response.body)['errors']).to_not be_blank

        tag.reload
        expect(tag.name).to_not eq invalid_params[:name]
        expect(tag.color).to_not eq invalid_params[:color]
      end
    end
  end

  context 'DELETE /v1/tags/:id' do
    let!(:tag) { FactoryBot.create(:tag) }

    it 'destroys requested tag' do
      expect do
        delete "/v1/tags/#{tag.id}"
      end.to change(Tag, :count).by(-1)

      expect(response).to have_http_status(:no_content)

      expect(Tag.find_by(id: tag.id)).to be_nil
    end
  end
end
