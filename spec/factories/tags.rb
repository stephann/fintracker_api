FactoryBot.define do
  factory :tag do
    name { Faker::Address.city }
    color { Faker::Color.hex_color }
  end
end
