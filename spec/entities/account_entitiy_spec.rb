require 'rails_helper'

RSpec.describe AccountEntity do
  let(:account) { FactoryBot.create(:account) }
  let(:account_entity) { AccountEntity.new(account) }
  let(:serializable_hash) { account_entity.serializable_hash }

  describe 'Fields' do
    it 'exposes id' do
      expect(serializable_hash[:id]).to eq account.id
    end

    it 'exposes name' do
      expect(serializable_hash[:name]).to eq account.name
    end

    it 'exposes balance' do
      expect(serializable_hash[:balance]).to eq account.balance.to_d.to_s
    end

    it 'exposes formatted_balance' do
      expect(serializable_hash[:formatted_balance]).to eq account.balance.formatted
    end

    it 'exposes color' do
      expect(serializable_hash[:color]).to eq account.color
    end
  end
end
