module Stefin
  module V1
    class Base < Grape::API
      version 'v1', using: :path

      mount Stefin::V1::Accounts
      mount Stefin::V1::Records
      mount Stefin::V1::Tags
    end
  end
end
