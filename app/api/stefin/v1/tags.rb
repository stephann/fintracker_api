module Stefin
  module V1
    class Tags < Grape::API
      # GET /v1/tags
      desc 'List tags'

      get '/tags' do
        tags = Tag.all
        present :tags, tags, with: TagEntity
      end

      # GET v1/tags/:id
      desc 'Show tag details'

      params do
        requires :id, type: String
      end

      get '/tags/:id' do
        tag = Tag.find(params[:id])
        present :tag, tag, with: TagEntity
      end

      # POST v1/tags
      desc 'Create a tag'

      params do
        requires :tag, type: Hash do
          requires :name, type: String
          requires :color, type: String
        end
      end

      post '/tags' do
        tag = Tag.new(declared(params)[:tag])

        if tag.save
          present :tag, tag, with: TagEntity
          status :created
        else
          present :errors, tag.errors
          status :unprocessable_entity
        end
      end

      # PUT /v1/tags/:id
      params do
        requires :id, type: String
        requires :tag, type: Hash do
          optional :name, type: String
          optional :color, type: String
          at_least_one_of :name, :color
        end
      end

      put '/tags/:id' do
        tag = Tag.find(params[:id])

        if tag.update(declared(params, include_missing: false)[:tag])
          present :tag, tag, with: TagEntity
          status :ok
        else
          present :errors, tag.errors
          status :unprocessable_entity
        end
      end

      # DELETE /v1/tags/:id
      params do
        requires :id, type: String
      end

      delete '/tags/:id' do
        tag = Tag.find(params[:id])

        tag.destroy

        status :no_content
      end
    end
  end
end
