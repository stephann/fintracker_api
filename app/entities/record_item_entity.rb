class RecordItemEntity < Grape::Entity
  expose :id, documentation: {
    type: String
  } do |record_item|
    record_item.id.to_s
  end

  expose :name, documentation: {
    type: String
  }

  expose :description, documentation: {
    type: String
  }

  expose :amount, documentation: {
    type: String
  } do |record_item|
    record_item.amount.to_d.to_s
  end

  expose :formatted_amount, documentation: {
    type: String
  } do |record_item|
    record_item.amount.formatted
  end

  expose :tags, using: TagEntity, documentation: {
    type: Array
  }
end
