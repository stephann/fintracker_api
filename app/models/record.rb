class Record < ApplicationRecord
  belongs_to :account
  has_many :record_items, inverse_of: :record, dependent: :destroy
  has_many :tags, -> { distinct }, through: :record_items

  accepts_nested_attributes_for :record_items, allow_destroy: true

  as_enum :group, expense: -1, income: 1
  monetize :amount_cents, numericality: {
    greater_than: 0
  }

  before_validation :assign_calculated_amount_to_amount
  after_save :update_account_balance, if: :amount_cents_changed?
  after_destroy :update_account_balance

  validates_presence_of :occurrence_date
  validates_presence_of :record_items
  validates_presence_of :group

  def self.total_amount
    sum(&:amount)
  end

  def assign_calculated_amount_to_amount
    self.amount = calculated_amount
  end

  def calculated_amount
    record_items.map(&:amount).sum
  end

  def update_account_balance
    account.update_balance unless account.marked_for_destruction?
  end
end
