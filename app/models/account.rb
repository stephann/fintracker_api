class Account < ApplicationRecord
  has_many :records, dependent: :destroy

  monetize :balance_cents

  validates_presence_of :name
  validates_presence_of :balance_cents
  validates_presence_of :balance_currency
  validates_presence_of :color
  validates_uniqueness_of :name, case_sensitive: false
  validates_format_of :color, with: /\A#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})\z/

  def update_balance
    update(balance: calculated_balance)
  end

  def calculated_balance
    records.incomes.total_amount - records.expenses.total_amount
  end
end
