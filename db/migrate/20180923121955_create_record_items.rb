class CreateRecordItems < ActiveRecord::Migration[5.2]
  def change
    create_table :record_items, id: :uuid do |t|
      t.references :record, foreign_key: true, index: true, null: false, type: :uuid
      t.string :name, null: false
      t.string :description
      t.monetize :amount
    end
  end
end
