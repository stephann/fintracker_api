class CreateTags < ActiveRecord::Migration[5.2]
  def change
    create_table :tags, id: :uuid do |t|
      t.citext :name, null: false, unique: true
      t.string :color, null: false
    end
  end
end
